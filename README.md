# Readme for State Pattern Library                             {#mainpage}

This is the readme for State Pattern example library.
>**WARNING**: development stage is far from complete. I need a lot to do.
## Prerequisites:

- CMake 3.11 or better; 
- 3.14+ highly recommended.
- A C++17 compatible compiler
- Git
- Doxygen (optional)

## Usage
1. Describe events (apps/events/events.h);
2. Describe states (apps/states/*.h, *.cpp);
3. Describe data   (apps/data/*.h, *.cpp);
4. Describe state_table (apps/main.cpp);
4. build & run;
5. Input numbers in stdin, where the number represents an index of an event described in enum event_e (events/events.h);
6. Analyse the result

