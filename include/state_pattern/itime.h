#pragma once

#include <chrono>

using std::chrono::steady_clock;

class ITime {
 public:
  virtual size_t time()         = 0;
  virtual void setTime(size_t)  = 0;
  virtual steady_clock::duration interval()        = 0;
  virtual void setInterval(steady_clock::duration) = 0;
  virtual void update()         = 0;
  virtual ~ITime() {}
};
