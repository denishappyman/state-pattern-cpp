#pragma once

#include <string>

class IStateWithName {
public:
  virtual std::string stateName()         = 0;
  virtual ~IStateWithName() {}
};
