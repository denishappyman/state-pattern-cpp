#pragma once

#include <iostream>
#include <list>
#include <vector>
#include "state_pattern/icommandprocessor.h"
#include "state_pattern/colors.h"

using std::cout;
using std::endl;
using std::list;
using std::vector;

class IState;
class IMachine;

// CCommandProcessor means Concrete Command Processor
class CCommandProcessor : virtual public ICommandProcessor {
 private:
  list<istate_func_p> commandQueue;
  int last_err = 0;
//   cpstatus means Command Processor Status
  size_t cpstatus;

 public:
  enum {READY, RUNNING/*, SUSPENDED, DISABLED*/};
  CCommandProcessor():
    last_err(0),  cpstatus(READY) {}
//    command processing
  virtual int getLastError() {  return this->last_err;  }

  virtual void push_command(int (IState::*command)(IMachine *)) {
    commandQueue.push_back(command);
  }

  virtual void pop_commands(IState *currentState, IMachine *m) {
//      Могут быть необработанные команды, даже если не было
//     других events в этой итерации
    size_t commands_executed = 0;
    while(!(commandQueue.empty())) {
      cout << "commandQueue.size() : "
           << BOLD( << commandQueue.size() <<) << endl;
      last_err = (currentState->*commandQueue.front())(m);
      commandQueue.pop_front();
      ++commands_executed;
//      TODO(denis): добавить блокировки и прерывание цикла
      if(last_err != 0) {
        cout << "last_command retured error = "
             << BOLD(FRED( << last_err <<)) << endl;
//        Очищать список команд при ошибке
        clear_commands();
//        TODO(denis): Здесь мы должны откатить Data назад
        break;
      }
//      TODO(denis): ограничитель выполненных команд должен задаваться извне
      if(commands_executed > 0) {  break;  }
    }
    this->cpstatus = commandQueue.empty() ? READY : RUNNING;
  }

  virtual void clear_commands() {  commandQueue.clear();  }

  virtual size_t getCStatus() {  return this->cpstatus;  }

  virtual ~CCommandProcessor() {}
};
