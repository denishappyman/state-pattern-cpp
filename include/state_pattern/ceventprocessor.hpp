#pragma once

#include <iostream>
#include <list>
#include <vector>
#include <regex>
#include <mutex>
#include <utility>
#include "state_pattern/ieventprocessor.h"
#include "state_pattern/imachine.h"
#include "state_pattern/colors.h"

using std::cout;
using std::cin;
using std::endl;
using std::list;
using std::vector;
using std::mutex;
using std::lock_guard;
using std::pair;
using std::regex;

class IState;
class IMachine;

class CEventProcessor : public virtual IEventProcessor {
 private:
  vector<list<istate_func_p> > state_table;
  list<pair <size_t /*tick*/, size_t /*event*/> > eventQueue;
  list<pair <size_t /*tick*/, size_t /*event*/> > eventQueueLog;
  mutex eventQueue_mutex;
  size_t _tick;
  size_t _oldest_log_tick;  // это для вычисления времени для событий


 public:
  explicit CEventProcessor(const vector<list<istate_func_p> > s_t):
    state_table(s_t),
    _tick(0),  // TODO(denis): _tick must be injectable
    _oldest_log_tick(0) {}

//  event processing
  size_t input_event(string input = "") {
    regex num_regex("[0-9]+");
    const size_t _MAX_EVENTS = state_table.size();
    const size_t _ON_REPLAY = _MAX_EVENTS > 0 ? _MAX_EVENTS - 1 : _MAX_EVENTS;
    size_t event = _MAX_EVENTS;

    if(!(regex_match(input, num_regex))) {
//    cout << "Error. Incorrect input. Exit..\n";
      return event;
    }
    event = static_cast<size_t>(atoi(input.c_str()));
    if(event >= _MAX_EVENTS) {
      cout << FRED("Error. Incorrect input. Try again..\n ");
      return event;
    }
    if(event == _ON_REPLAY) {
//      TODO(denis): продумать обработку ON_REPLAY внутри самой машины
//      replayEvents(/*this*/);
//      TODO(denis): управление не возвращаем пока реплей не кончится
      return event;
    }
    push_event(this->time(), event);
    return event;
  }


  void push_event(size_t new_tick, size_t event) {
    // defending access
    lock_guard<mutex> guard(eventQueue_mutex);
    auto new_pair = std::make_pair(new_tick, event);
    eventQueue.push_back(new_pair);
    eventQueueLog.push_back(new_pair);
  }

  void pop_events(ICommandProcessor *m) {
//   defending access
    lock_guard<mutex> guard(eventQueue_mutex);
    size_t events_popped = 0;
    while(!(eventQueue.empty())) {
//    cout << "eventQueue.size() : " << eventQueue.size() << endl;
      size_t event_tick = eventQueue.front().first;
      if(this->time() >= event_tick) {
//       Время для события пришло
        size_t event_index = eventQueue.front().second;
        cout << BOLD("\nMachine: emitting event [ "
             << event_index << " ]") << endl;
//        TODO(denis): onEvent должен возвращать event_index
//        и метку времени, когда он был инициирован.
        onEvent(m, event_index);
        eventQueue.pop_front();
        ++events_popped;
      } else {  break;  }
//      TODO(denis): добавить блокировки и прерывание цикла
      if(events_popped > 0) {  break;  }
    }
  }

  void onEvent(ICommandProcessor *m, size_t event_index) {
//   Выбираем список команд из таблицы
    for(auto command : state_table[event_index]) { m->push_command(command); }
  }

  void replayEvents(IDataProcessor *dp) {
// здесь он должен загрузить стейт
    dp->restoreData();
//  lock_guard<mutex> guard(eventQueue_mutex);

//  обрабатывать очередь истории событий
//  1. Скопировать eventQueueLog (переместить)
    auto eventQueueLogCopy(std::move(eventQueueLog));
//  2. Копию положить в eventQueue, только со смещением по времени

//    TODO(denis): расписать как правильно рассчитывается смещение по времени
    while(!eventQueueLogCopy.empty()) {
//       у первого аргумента настоящее время + время сработки в прошлом
      push_event(  this->time() + eventQueueLogCopy.front().first
                 - this->_oldest_log_tick,
                   eventQueueLogCopy.front().second);
      eventQueueLogCopy.pop_front();
//        3. Очистить eventQueueLog
      eventQueueLog.clear();
//       FIXME: эта точка отсчета должна синхронизироваться
//      после окончания реплея
    }
    this->_oldest_log_tick = this->time();
  }

  void setTime(size_t new_tick) {  _tick = new_tick;  }

  size_t time() {  return _tick;  }

  virtual ~CEventProcessor() {}
};

