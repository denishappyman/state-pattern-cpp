#pragma once

#include <string>
#include "istateprocessor.h"
#include "colors.h"

class CStateProcessor : virtual public IStateProcessor {
 private:
  class IState *_currentState;

 public:
  explicit CStateProcessor(IState *initial):
    _currentState(initial) {}
  virtual void setCurrent(IState *s) {  _currentState = s;  }
  virtual IState *State()         {  return _currentState;  }
//  Мы не хотим чтобы library знала о деталях IState и потомков,
//  поэтому пока тут forward declaration. Но есть слабое место.
//  IState обязательно должен отнаследоваться от IStateWithName,
//  иначе метод не сработает.
//  TODO(denis): Придумать другой способ, без кастов.
  virtual string getStateName() {
    return reinterpret_cast<IStateWithName*>(_currentState)->stateName();
  }
  virtual ~CStateProcessor()         {  delete this->_currentState;  }
};
