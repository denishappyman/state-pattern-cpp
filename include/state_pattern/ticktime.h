#pragma once

#include "itime.h"

class TickTime : public ITime {
 private:
  size_t _time;
  steady_clock::duration _interval;
 public:
  TickTime();
  size_t time();
  void setTime(size_t new_time);
  steady_clock::duration interval();
  void setInterval(steady_clock::duration new_interval);
  void update();
  ~TickTime();
};
