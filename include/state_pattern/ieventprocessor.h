#pragma once

#include <cstddef>
#include <string>

using std::string;

class IEventProcessor {
public:
//  event processing
  virtual size_t input_event(string)											= 0;
  virtual void push_event(size_t /*tick*/,
                          size_t /*event_index*/)         = 0;
  virtual void pop_events(class  ICommandProcessor *)     = 0;
  virtual void onEvent(class ICommandProcessor *, size_t) = 0;
  virtual void replayEvents(class IDataProcessor *)       = 0;
//  time processing
  virtual void setTime(size_t new_tick)                   = 0;
  virtual size_t time()                                   = 0;

  virtual ~IEventProcessor() {}
};
