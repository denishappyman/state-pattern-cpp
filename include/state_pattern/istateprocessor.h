#pragma once

#include "istatewithname.h"
#include <string>

using std::string;

class IState;

class IStateProcessor {
public:
//  event processing
  virtual void setCurrent(IState *s) = 0;
  virtual IState *State()         = 0;
//  virtual string getStateName() = 0;
  virtual ~IStateProcessor() {}
};
