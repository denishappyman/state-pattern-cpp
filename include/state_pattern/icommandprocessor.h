#pragma once

#include <cstddef>

class IState;
class IMachine;
using istate_func_p = int(IState::*)(IMachine *);

class ICommandProcessor {
public:
  //  command processing
  virtual int getLastError()                      = 0;
  virtual void push_command(istate_func_p)        = 0;
  virtual void pop_commands(IState *, IMachine *) = 0;
  virtual void clear_commands()                   = 0;
  virtual size_t getCStatus()                     = 0;
  virtual ~ICommandProcessor() {}
};
