#pragma once

class Data;

class IDataProcessor {
public:
//  data processing
  virtual Data data()   		  = 0;
  virtual Data &modifyData()  = 0;
  virtual void backupData()   = 0;
  virtual void restoreData()  = 0;
  virtual void displayData()  = 0;
  virtual ~IDataProcessor() {}
};

