#pragma once

#include <iostream>
#include "state_pattern/idataprocessor.h"
#include "state_pattern/colors.h"
#include "../apps/data/data.h"

using std::cout;

class CDataProcessor : virtual public IDataProcessor {
 private:
//  TODO(denis) Replace to IData
  Data _data;
  Data _last_data;
 public:
//  data processing
  CDataProcessor(Data d):
    _data(d)
    /*_last_data(d)*/ {
//    TODO(denis): Доработать способ копирвания обьектов
    backupData();
  }

  virtual Data  data() {  return _data;  }
//  TODO(denis) делать бэкап данных перед передачей
//  ссылки на объект для изменения.
  virtual Data & modifyData() {  return _data;  }

  void backupData() {
    cout << "backupData()\n";
//    TODO(denis): доработать способ копирования обьектов
    this->_last_data=this->_data;
  }

  void restoreData() {
    cout << "restoreData()\n";
//     TODO(denis) std::move
    this->_data = this->_last_data;
//      FIXME после этого мы теряем данные
  }
//   TODO(denis) implement displayData()
//  virtual void displayData() {}
  virtual ~CDataProcessor() {}
};

