#pragma once

#include <iostream>
#include <vector>
#include <list>
#include <mutex>
#include "imachine.h"

#include "cdataprocessor.hpp"
#include "cstateprocessor.hpp"
#include "ceventprocessor.hpp"
#include "ccommandprocessor.hpp"


using std::vector;
using std::mutex;
using std::lock_guard;

class Data;

class Machine : public CCommandProcessor,
                public CStateProcessor,
                public CEventProcessor,
                public CDataProcessor,
                public IMachine {
 public:
  Machine(IState *initialState, Data data,
          const vector<list<istate_func_p> > state_table);

  void displayData();
  int execute(size_t new_tick);
  ~Machine();
};
