#pragma once

#include <cstddef>
#include "state_pattern/idataprocessor.h"
#include "state_pattern/istateprocessor.h"
#include "state_pattern/ieventprocessor.h"
#include "state_pattern/icommandprocessor.h"

class IState;

using istate_func_p = int(IState::*)(IMachine *);

class IMachine : virtual public IDataProcessor,
                 virtual public IStateProcessor,
                 virtual public IEventProcessor,
                 virtual public ICommandProcessor {
 public:
  virtual int execute(size_t) = 0;
  virtual ~IMachine() {}
};
