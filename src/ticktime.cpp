#include "state_pattern/ticktime.h"

using namespace std::literals::chrono_literals;

TickTime::TickTime():
  _time(0),
  _interval(1s) {
}

size_t TickTime::time() {  return _time;  }

void TickTime::setTime(size_t new_time) {
  if(_time < new_time) {  _time = new_time;  }
}

steady_clock::duration TickTime::interval() {  return _interval;  }

void TickTime::setInterval(steady_clock::duration new_interval) {
  _interval = new_interval;
}

void TickTime::update() {
  // TODO(denis): мютекс и инкремент
  setTime(time() + 1);
}

TickTime::~TickTime() {}
