#include "state_pattern/machine.h"
#include "state_pattern/colors.h"

Machine::Machine(IState *initialState, Data data,
                 const vector<list<istate_func_p> > state_table):
  CStateProcessor(initialState),
  CEventProcessor(state_table),
  CDataProcessor(data) {
  cout << '\n';
}

void Machine::displayData() {
  cout << BOLD("\n  Data members:\n");
  cout << "   StateName: ";
  cout << this->getStateName() << endl;
  cout << "   last_err: ";
  cout << this->getLastError() << endl;
  cout << endl;
  this->data().display();
}

int Machine::execute(size_t new_tick) {
  this->setTime(new_tick);
//  TODO(denis): рассмотреть целесообразность блокировки mode != RUNNING,
//  т.к. возможен event отменяющий обработку отложенных event
  if(this->getCStatus() != RUNNING) {
//     TODO(denis): перед эмитом eventa запомнить данные, чтобы можно
//     было откатиться.
//     TODO(denis): pop_events должен возвращать количество eventoв.
    this->pop_events(this);
  }
  this->pop_commands(this->State(), this);
  return this->getCStatus();
}

Machine::~Machine() {}
