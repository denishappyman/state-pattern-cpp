#include <iostream>
#include <list>
#include <regex>
#include <thread>
#include "state_pattern/machine.h"
#include "state_pattern/ticktime.h"
#include "state_pattern/colors.h"
#include "events/events.h"
#include "states/disabledstate.h"
#include "states/istate.h"
#include "data/data.h"

using namespace std::literals::chrono_literals;
using std::chrono::steady_clock;
using std::regex;
using std::regex_match;
using std::vector;
using std::list;
using std::thread;
using std::cin;
using std::cout;

/*  TODO(denis): решить следующие проблемы:
 * - Стейты знают друг про друга.
 * - Не обеспечивается потокобезопасность...
*/

// this funcion invokes from another thread
void run(IMachine *fsm,
         steady_clock::duration cycle_time_limit);

int main() {
  IState *disabledState = new DisabledState();
  Data data;
  using IS = IState;
  vector<list<istate_func_p> > state_table = {
    {&IS::onDisable},
    {&IS::onEnable},
    {&IS::onNewMeasure,         &IS::onNext},
    {&IS::onClearAll,           &IS::onNext},
    {&IS::onMakeAverage,        &IS::onNext},
    {&IS::onOilLimitCalculated, &IS::onNext},
    {&IS::onMakeAverage,        &IS::onNext,
     &IS::onCalculateOil,       &IS::onNext,
     &IS::onClearAll,           &IS::onNext},
    {&IS::onNext},
    {&IS::onCalculateOil,       &IS::onNext},
    {&IS::onNext},
    {&IS::onNext}
  };

  Machine fsm(disabledState, data, std::move(state_table));
  // run() имитирует вызов сервера
  thread thread1(run, &fsm, 10ms);
  int event = 0;
//  TODO(denis): Чтение из стандартного ввода сделать опцией
  while(1) {
    string input("");
    cin >> input;
    event = fsm.input_event(input);
    if(event >= MAX_EVENTS) {
      cout << FRED("Error. Incorrect input. Try again..\n ");
      continue;
    }
    if(event == ON_REPLAY) {
      // TODO(denis): продумать обработку ON_REPLAY внутри самой машины
      fsm.replayEvents(&fsm);
      continue;
    }
  }
  thread1.join();
  getchar();
  return 0;
}

void run(IMachine *fsm, steady_clock::duration cycle_time_limit) {
  using TP = steady_clock::time_point;
  using DR = steady_clock::duration;
  // Имитация времени в тиках
  TickTime tickTime;
  int status = 0, last_status = -1;
  while(1) {
//    TODO(denis): останавливать выполнение, если
//    время цикла превышает предельное
    size_t tick = tickTime.time();
//    TODO(denis): Распилить на несколько функций
//    1. вести подсчет времени выполнения execute()
    TP cycle_time_start = steady_clock::now();
    status = fsm->execute(tick);
    TP cycle_time_end = steady_clock::now();
    DR actual_cycle_time = cycle_time_end - cycle_time_start;

    DR full_cycle_time = actual_cycle_time;
    while(full_cycle_time < cycle_time_limit) {
      std::this_thread::sleep_for(1ms);
      full_cycle_time = steady_clock::now() - cycle_time_start;
    }
//    2. Вывод в консоль результатов
    string mode_str = status ? BOLD(FYEL("RUNNING")) : BOLD(FGRN("READY"));
    if(last_status!= status) { fsm->displayData(); }
    if(last_status != status || status) {
      cout << "Machine::execute() returned : "
           << mode_str << endl
           << "Actual cycle_time : "
           << std::chrono::duration_cast<std::chrono::microseconds>(
                actual_cycle_time).count()
           << " mks\n\n";
    }
    last_status = status;
    tickTime.update();
  }
}
