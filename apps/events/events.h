#pragma once

// List of event indexes;
typedef enum {
  ON_DISABLE,
  ON_ENABLE,
  ON_NEW_MEASURE,
  ON_CLEAR_ALL,
  ON_MAKE_AVERAGE,
  ON_OIL_LIMIT_CALCULATED,
  ON_INPUT_IMPULSE,
  ON_NEXT,
  ON_CALCULATE_OIL,
  ON_REPLAY,
  MAX_EVENTS
//  ON_CLEAR,
} event_e;

