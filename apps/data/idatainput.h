#pragma once

#include <iostream>
#include <list>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::list;

class IDataInput {
public:
  virtual int inputInt(size_t index)  = 0;
//  virtual int &modifyInputInt(size_t index) = 0;
  virtual float inputFloat(size_t index) = 0;
//  virtual float &modifyInputFloat(size_t index) = 0;
  virtual string inputIntName(size_t index)  = 0;
  virtual string inputFloatName(size_t index) = 0;
  virtual void display() = 0;
};
