#pragma once

//TODO(denis): rename as *.hpp

#include <iostream>
#include <list>
#include <string>
#include "datainput.h"
#include "idata.h"

using std::cout;
using std::endl;
using std::string;
using std::list;

enum {OUTPUT, MAX_INT};
enum {LAST_AVERAGE, V_OIL, MAX_FLOAT};

class Data : public DataInput, public IData {
 private:
//   Есть два вида данных: входные  и  выходные.
//   Входные данные меняются не зависимо от нас.
//   Выходные данные меняем мы.

  string int_params_names[MAX_INT]     = { "Output" };
  int int_params[MAX_INT]              = {        0 };
  string float_params_names[MAX_FLOAT] = { "Last_average", "V_oil" };
  float float_params[MAX_FLOAT]        = {          -1.0f,    0.0f };
//  TODO(denis): придумать cпособ, чтобы обрабатывать стандартными методами.
  list<float> waterMeasures            = { 1, 2, 3 };

 public:
  Data() {}
  // TODO(denis): std::nullopt здесь пригодится
  // TODO(denis): Проверять выход за границы
  int getInt(size_t index)           {  return int_params[index];  }
  int &modifyInt(size_t index)       {  return int_params[index];  }
  float getFloat(size_t index)       {  return float_params[index];  }
  float &modifyFloat(size_t index)   {  return float_params[index];  }
  string getIntName(size_t index)    {  return int_params_names[index];  }
  string getFloatName(size_t index)  {  return float_params_names[index];  }
  void setInt(size_t index, int new_val)     {  int_params[index]   = new_val;  }
  void setFloat(size_t index, float new_val) {  float_params[index] = new_val;  }
  list<float> &getRefWaterMeasures() {  return waterMeasures;  }
  void display() {
//    cout << "\n  Data members:\n";
//    invoked parent's method display()
    DataInput::display();
    for(size_t i = 0; i < MAX_INT; ++i)   {
      cout << "   " << getIntName(i) << ": " << getInt(i) << endl;
    }
    for(size_t i = 0; i < MAX_FLOAT; ++i) {
      cout << "   " << getFloatName(i) << ": " << getFloat(i) << endl;
    }
    cout << "   measures: ";
    for(auto measure : getRefWaterMeasures()) {   cout << measure << " ";  }
    cout << endl;
  }
};
