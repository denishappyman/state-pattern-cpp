#pragma once

#include <iostream>
#include <list>
#include <string>
#include "idatainput.h"

using std::cout;
using std::endl;
using std::string;
using std::list;

enum {MAX_INPUT_INT};
enum {CURRENT_WATER, FLOWMETER_VOLUME, MAX_INPUT_FLOAT};

class DataInput : virtual public IDataInput {
 private:
//   Входные данные меняются не зависимо от нас внешними пользователями.
  string input_int_params_names[MAX_INPUT_INT]     = { };
  int input_int_params[MAX_INPUT_INT]              = { };
  string input_float_params_names[MAX_INPUT_FLOAT] = { "Current_water", "Flowmeter_volume" };
  float input_float_params[MAX_INPUT_FLOAT]        = {           10.0f,            1000.0f };
 public:
  DataInput() {}
  // TODO(denis): std::nullopt здесь пригодится
  // TODO(denis): Проверять выход за границы
  int inputInt(size_t index)           {  return input_int_params[index];  }
  float inputFloat(size_t index)       {  return input_float_params[index];  }
  string inputIntName(size_t index)    {  return input_int_params_names[index];  }
  string inputFloatName(size_t index)  {  return input_float_params_names[index];  }

  void display() {
//    cout << "\n  Data members:\n";
    for(size_t i = 0; i < MAX_INPUT_INT; ++i)   {  cout << "   " << inputIntName(i) << ": " << inputInt(i) << endl;  }
    for(size_t i = 0; i < MAX_INPUT_FLOAT; ++i) {  cout << "   " << inputFloatName(i) << ": " << inputFloat(i) << endl;  }
  }
};
