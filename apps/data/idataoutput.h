#pragma once

#include <iostream>
#include <list>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::list;

class IDataOutput {
public:
  virtual int getInt(size_t index)  = 0;
  virtual int &modifyInt(size_t index)      = 0;
  virtual float getFloat(size_t index)      = 0;
  virtual float &modifyFloat(size_t index)  = 0;
  virtual string getIntName(size_t index)   = 0;
  virtual string getFloatName(size_t index) = 0;
};
