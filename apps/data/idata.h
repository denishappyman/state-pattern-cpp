#pragma once

#include <iostream>
#include <list>
#include <string>
#include "idatainput.h"

using std::cout;
using std::endl;
using std::string;
using std::list;

class IData : virtual public IDataInput {
 public:
  virtual list<float> &getRefWaterMeasures() = 0;
  virtual void display() = 0;
};
