#include "disabledstate.h"
#include "idlestate.h"

DisabledState::DisabledState():
  BaseState("DisabledState") {}

DisabledState::~DisabledState() {}

int DisabledState::onEnable(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new IdleState());
  cout << "   from ";
  delete this;
  return 0;
}
