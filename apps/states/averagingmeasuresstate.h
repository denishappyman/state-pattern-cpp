#pragma once

#include <list>
#include <functional>
#include "state_pattern/imachine.h"
#include "basestate.h"

class AveragingMeasuresState: public BaseState {
 private:
  std::list<float> &_waterMeasures;
  float _average;
  float getAverageMeasures(std::list<float> const &waterMeasures);
 public:
  // TODO(denis): make first argument as const reference
  AveragingMeasuresState(std::list<float> &waterMeasures,
                         std::function<void(float)> callback);
  ~AveragingMeasuresState();
  virtual int onNext(IMachine *m);
};
