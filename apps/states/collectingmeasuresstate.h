#pragma once

#include <functional>
#include <list>
#include "state_pattern/imachine.h"
#include "basestate.h"

// протестировать
class CollectingMeasuresState: public BaseState {
 public:
  CollectingMeasuresState(std::function<void(int)> callback);
  ~CollectingMeasuresState();
  virtual int onNext(IMachine *m);
};
