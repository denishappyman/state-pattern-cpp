#include "idlestate.h"
#include "disabledstate.h"
#include "collectingmeasuresstate.h"
#include "clearingmeasuresstate.h"
#include "averagingmeasuresstate.h"
#include "setoutputstate.h"
#include "calculateoilstate.h"
#include "inputimpulsestate.h"
#include "../data/data.h"

IdleState::IdleState():
  BaseState("IdleState") {
}

IdleState::~IdleState() {
}

int IdleState::onDisable(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new DisabledState());
  cout << "   from ";
  delete this;
  return 0;
}

int IdleState::onNewMeasure(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new CollectingMeasuresState(
    [m](int res) {  m->modifyData().getRefWaterMeasures().push_back(
                      m->data().inputFloat(CURRENT_WATER));  }  ));
  cout << "   from ";
  delete this;
  return 0;
}

int IdleState::onClearAll(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new ClearingMeasuresState(
    [m](int res) {  m->modifyData().getRefWaterMeasures().clear();  }  ));
  cout << "   from ";
  delete this;
  return 0;
}

int IdleState::onMakeAverage(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new AveragingMeasuresState(
    m->modifyData().getRefWaterMeasures(),
    [m](float res){  m->modifyData().setFloat(LAST_AVERAGE, res);  }  ));
  cout << "   from ";
  delete this;
  return 0;
}

int IdleState::onOilLimitCalculated(IMachine *m) {
  cout << "   going to ";
  // TODO(denis): инкремент отложенных импульсов
  m->setCurrent(new SetOutputState(
    [m](int res){  m->modifyData().setInt(OUTPUT, SET_ON);  }  ));
//  TODO(denis): придумать отключение дискретного выхода через N тиков
  cout << "   from ";
  delete this;
  return 0;
}

int IdleState::onCalculateOil(IMachine *m) {
  cout << "   going to ";
  m->setCurrent( new CalculateOilState(
    m->data().getFloat(LAST_AVERAGE),
    m->data().inputFloat(FLOWMETER_VOLUME),
    [m](float res){ m->modifyData().setFloat(V_OIL, res);  }  ));
  cout << "   from ";
  delete this;
  return 0;
}

int IdleState::onInputImpulse(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new InputImpulseState());
  cout << "   from ";
  delete this;
  return 0;
}
