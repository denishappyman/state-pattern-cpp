#pragma once

#include <iostream>
#include <string>
#include "istate.h"
#include "state_pattern/imachine.h"
#include "state_pattern/colors.h"

using std::string;
using std::cout;

class IMachine;

class BaseState : public IState {
  const string _stateName;
public:
  string stateName() {
    return _stateName;
  }
  virtual int onEnable(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onEnable() is not implemented\n");
    return 404;
  }

  virtual int onDisable(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onDisable() is not implemented\n");
    return 404;
  }

  virtual int onNewMeasure(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onNewMeasure() is not implemented\n");
    return 404;
  }

  virtual int onClearAll(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onClearAll() is not implemented\n");
    return 404;
  }

  virtual int onMakeAverage(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onMakeAverage() is not implemented\n");
    return 404;
  }

  virtual int onOilLimitCalculated(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onOilLimitCalculated() is not implemented\n");
    return 404;
  }

  virtual int onCalculateOil(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onCalculateOil is not implemented\n");
    return 404;
  }

  virtual int onInputImpulse(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onInputImpulse() is not implemented\n");
    return 404;
  }

  virtual int onNext(IMachine *m) {
    cout << FYEL("   " << stateName();
                 cout << ": onNext() is not implemented\n");
    return 404;
  }

  BaseState():
    _stateName("BaseState") {}
  explicit BaseState(std::string name):
    _stateName(name) {
    cout << "   " << stateName() << "-ctor ";
  }
  virtual ~BaseState() {
    cout << "   " << stateName() << "-dtor \n";
  }
};
