#include "collectingmeasuresstate.h"
#include "idlestate.h"
#include <list>


CollectingMeasuresState::CollectingMeasuresState(
    std::function<void(int)> callback
    ):
  BaseState("CollectingMeasuresState") {
  callback(0);
}

int CollectingMeasuresState::onNext(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new IdleState());
  cout << "   from ";
  delete this;
  return 0;
}

CollectingMeasuresState::~CollectingMeasuresState() {
}
