#include "setoutputstate.h"
#include "idlestate.h"

SetOutputState::SetOutputState(std::function<void(int)> callback):
  BaseState("SetOutputState") {
  callback(0);
}

SetOutputState::~SetOutputState() {}

int SetOutputState::onNext(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new IdleState());
  cout << "   from ";
  delete this;
  return 0;
}
