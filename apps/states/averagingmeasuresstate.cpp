#include "averagingmeasuresstate.h"
#include "idlestate.h"

AveragingMeasuresState::AveragingMeasuresState(std::list<float> &waterMeasures,
                                               std::function<void(float)> callback):
  BaseState("AveragingMeasuresState"),
  _waterMeasures(waterMeasures)
{
  _average = getAverageMeasures(_waterMeasures);
  callback(_average);
}

AveragingMeasuresState::~AveragingMeasuresState() {
}

float AveragingMeasuresState::getAverageMeasures(const std::list<float>
                                                 &waterMeasures) {
  float sum = 0;
  for (auto measure : waterMeasures) {
    sum += measure;
  }
  size_t size = waterMeasures.size();
  // TODO(denis): при делении на нуль должен вернуть std::nullopt
  return size == 0 ? 0 : sum / size;
}

int AveragingMeasuresState::onNext(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new IdleState());
  cout << "   from ";
  delete this;
  return 0;
}
