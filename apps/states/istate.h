#pragma once

#include <string>
#include "state_pattern/istatewithname.h"

class IMachine;

//  IState обязательно должен отнаследоваться от IStateWithName,
//  потому что в cstateprocessor.hpp мы кастим IState до IStateWithName.
//  TODO(denis): Продумать другой способ, без кастов, но без связности.
class IState : public IStateWithName {
public:
  virtual int onEnable(IMachine *m)       = 0;
  virtual int onDisable(IMachine *m)      = 0;
  virtual int onNewMeasure(IMachine *m)   = 0;
  virtual int onClearAll(IMachine *m)     = 0;
  virtual int onMakeAverage(IMachine *m)  = 0;
  virtual int onOilLimitCalculated(IMachine *m) = 0;
  virtual int onCalculateOil(IMachine *m) = 0;
  virtual int onInputImpulse(IMachine *m) = 0;
  virtual int onNext(IMachine *m)         = 0;
  virtual ~IState() {}
};
