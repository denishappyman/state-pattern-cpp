#pragma once

#include "state_pattern/imachine.h"
#include "basestate.h"

class DisabledState: public BaseState {
 public:
  DisabledState();
  ~DisabledState();
  virtual int onEnable(IMachine *m);
};
