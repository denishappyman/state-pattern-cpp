#pragma once

#include "state_pattern/imachine.h"
#include "basestate.h"

class InputImpulseState : public BaseState {
 public:
  InputImpulseState();
  ~InputImpulseState();
};
