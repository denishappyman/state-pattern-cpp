#pragma once

#include <functional>
#include <list>
#include "state_pattern/imachine.h"
#include "basestate.h"

class ClearingMeasuresState: public BaseState {
 private:
 public:
   ClearingMeasuresState(std::function<void(int)> callback);
  ~ClearingMeasuresState();
  virtual int onNext(IMachine *m);
};
