#pragma once

#include <functional>
#include "state_pattern/imachine.h"
#include "basestate.h"

enum {SET_OFF, SET_ON};

class SetOutputState: public BaseState {
 private:
 public:
  SetOutputState(std::function<void(int)> callback);
  ~SetOutputState();
  virtual int onNext(IMachine *m);
};

