#include "clearingmeasuresstate.h"
#include "idlestate.h"

ClearingMeasuresState::ClearingMeasuresState(std::function<void(int)> callback):
  BaseState("ClearingMeasuresState") {
  callback(0);
}

int ClearingMeasuresState::onNext(IMachine *m) {
  // как сделать так чтобы этот стейт после создания переходил в IdleState
  cout << "   going to ";
  m->setCurrent(new IdleState());
  cout << "   from ";
  delete this;
  return 0;
}

ClearingMeasuresState::~ClearingMeasuresState() {
}
