#pragma once

#include <functional>
#include "state_pattern/imachine.h"
#include "basestate.h"

class CalculateOilState: public BaseState {
 private:
  float _flowmeter_volume;
  float _averageWater;
  float _V_Oil;

  // возможно перенести в privatную секцию
  float calculateOil(const float averageWater, const float flowmeter_volume,
                     const float current_V_Oil);
 public:
  CalculateOilState(const float averageWater, const float flowmeter_volume,
                    std::function<void(float)> callback);
  ~CalculateOilState();
  virtual int onNext(IMachine *m);
};
