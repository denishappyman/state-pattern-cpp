#include "calculateoilstate.h"
#include "idlestate.h"

float CalculateOilState::calculateOil(const float averageWater,
                                      const float flowmeter_volume,
                                      const float current_V_Oil) {
  return current_V_Oil + (1 - averageWater / 100) * flowmeter_volume;
}

CalculateOilState::CalculateOilState(const float averageWater,
                                     const float flowmeter_volume,
                             std::function<void(float)> callback):
  BaseState("CalculateOilState"),
  _averageWater(averageWater),
  _flowmeter_volume(flowmeter_volume) {
  _V_Oil = calculateOil(_averageWater, _flowmeter_volume, _V_Oil);
  callback(_V_Oil);
}

CalculateOilState::~CalculateOilState() {
}

int CalculateOilState::onNext(IMachine *m) {
  cout << "   going to ";
  m->setCurrent(new IdleState());
  cout << "   going from ";
  delete this;
  return 0;
}
