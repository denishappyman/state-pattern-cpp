#pragma once

#include "state_pattern/imachine.h"
#include "basestate.h"

class IdleState: public BaseState {
public:
  virtual int onDisable(IMachine *m);
  virtual int onNewMeasure(IMachine *m);
  virtual int onClearAll(IMachine *m);
  virtual int onMakeAverage(IMachine *m);
  virtual int onOilLimitCalculated(IMachine *m);
  virtual int onCalculateOil(IMachine *m);
  virtual int onInputImpulse(IMachine *m);
  IdleState();
  ~IdleState();
};
